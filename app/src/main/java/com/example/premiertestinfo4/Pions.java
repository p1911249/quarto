package com.example.premiertestinfo4;

public class Pions {
    public boolean petit;
    public boolean clair;
    public boolean rond;
    public boolean vide;
    boolean isPlaced = false;


    Pions (boolean petit, boolean clair, boolean rond, boolean vide){
        this.petit = petit;
        this.clair = clair;
        this.rond = rond;
        this.vide = vide;

    }

    Pions () {
        this.petit = false;
        this.clair = false;
        this.rond = false;
        this.vide = false;
    }

    public void SetPlaced(){
        this.isPlaced = true;
    }

    public void UnsetPlaced(){
        this.isPlaced = false;
    }

}
