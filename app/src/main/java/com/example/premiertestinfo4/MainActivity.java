package com.example.premiertestinfo4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button regles;
    private Button sortie;
    private Button newgame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.regles = (Button) findViewById(R.id.regles);
        this.sortie = (Button) findViewById(R.id.exit);
        this.newgame = (Button) findViewById(R.id.newgame);

        sortie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                System.exit(0);
            }
        });

        regles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent afficherregles = new Intent(getApplicationContext(), ReglesDuJeu.class);
                startActivity(afficherregles);
            }
        });

        newgame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent nouvellepartie = new Intent(getApplicationContext(), Jeu.class);
                startActivity(nouvellepartie);
            }
        });
    }
}