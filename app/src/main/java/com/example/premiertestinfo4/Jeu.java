package com.example.premiertestinfo4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;
import android.widget.TextView;

public class Jeu extends AppCompatActivity implements View.OnClickListener {

    ImageButton selection;

    Pions[] pion;
    ImageButton [] btn_plateau;
    Plateau plateau;
    ImageButton derniereCaseActualisee;
    TextView player;

    Boolean activePlayer;
    //true pour joueur 1
    //false pour joueur 2

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jeu);

        launchGame();
    }

    void launchGame () {
        btn_plateau = new ImageButton[16];
        for (int i = 0; i < 16; i++) {
            String btn_str = "btn_case_" + (i);
            int btn_id = getResources().getIdentifier(btn_str, "id", getPackageName());
            ImageButton button = (ImageButton) findViewById(btn_id);
            btn_plateau[i] = button;
            button.setOnClickListener(this);
        }


        plateau = new Plateau(btn_plateau);

        //initialisation des pions
        pion = new Pions[16];
        pion[0] = new Pions(false, false, false, true);
        pion[1] = new Pions(true, false, false, true);
        pion[2] = new Pions(false, false, true, true);
        pion[3] = new Pions(true, false, true, true);
        pion[4] = new Pions(false, false, false, false);
        pion[5] = new Pions(true, false, false, false);
        pion[6] = new Pions(false, false, true, false);
        pion[7] = new Pions(true, false, true, false);
        pion[8] = new Pions(false, true, false, true);
        pion[9] = new Pions(true, true, false, true);
        pion[10] = new Pions(false, true, true, true);
        pion[11] = new Pions(true, true, true, true);
        pion[12] = new Pions(false, true, false, false);
        pion[13] = new Pions(true, true, false, false);
        pion[14] = new Pions(false, true, true, false);
        pion[15] = new Pions(true, true, true, false);


        for (int i = 0; i < 16; i++) {
            String str_btn = "btn_pion_" + (i);
            int btn_id = getResources().getIdentifier(str_btn, "id", getPackageName());
            ImageButton button = (ImageButton) findViewById(btn_id);
            button.setOnClickListener(this);
            button.setTag(pion[i]);
        }

        activePlayer = true;
        player = (TextView) findViewById(R.id.joueur);
        player.setText("Tour du " + getplayernb());

    }

    public void resetGame(View v) {
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_pion_0:
            case R.id.btn_pion_1:
            case R.id.btn_pion_2:
            case R.id.btn_pion_3:
            case R.id.btn_pion_4:
            case R.id.btn_pion_5:
            case R.id.btn_pion_6:
            case R.id.btn_pion_7:
            case R.id.btn_pion_8:
            case R.id.btn_pion_9:
            case R.id.btn_pion_10:
            case R.id.btn_pion_11:
            case R.id.btn_pion_12:
            case R.id.btn_pion_13:
            case R.id.btn_pion_14:
            case R.id.btn_pion_15:
                if (selection != null) return;
                else {
                    selection = (ImageButton) v;
                    selection.setBackgroundColor(Color.BLUE);
                    activePlayer = !activePlayer;
                    player.setText("Tour du " + getplayernb());
                }


                break;

            case R.id.btn_case_0:
            case R.id.btn_case_1:
            case R.id.btn_case_2:
            case R.id.btn_case_3:
            case R.id.btn_case_4:
            case R.id.btn_case_5:
            case R.id.btn_case_6:
            case R.id.btn_case_7:
            case R.id.btn_case_8:
            case R.id.btn_case_9:
            case R.id.btn_case_10:
            case R.id.btn_case_11:
            case R.id.btn_case_12:
            case R.id.btn_case_13:
            case R.id.btn_case_14:
            case R.id.btn_case_15:
                if (selection != null) {
                    String fullStr = getResources().getResourceName(v.getId());
                    String str = fullStr.substring(fullStr.lastIndexOf("/") + 10);
                    int btn_nbr;
                    try {
                        btn_nbr = Integer.parseInt(str);
                    } catch (NumberFormatException e) {
                        btn_nbr = 0;
                    }

                    selection.setX(v.getX());
                    selection.setY(v.getY());
                    selection.setBackgroundColor(Color.TRANSPARENT);
                    plateau.cases[btn_nbr].pion = (Pions) selection.getTag();
                    selection = null;
                    derniereCaseActualisee = (ImageButton) v;
                    checkWinner();
                } else {
                    String msg = "Vous n'avez pas sélectionné de pion";
                    Toast toast = Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT);
                    toast.show();
                    break;
                }
        }

    }

    void checkWinner () {
        boolean winner = false;

        //récupère le numéro de la derniere case actualisée
        String idFullStr = getResources().getResourceName(derniereCaseActualisee.getId());
        String id_str = idFullStr.substring(idFullStr.lastIndexOf("/") + 10);
        int id;
        try {
            id = Integer.parseInt(id_str);
        } catch (NumberFormatException e) {
            id = 0;
        }

        //cherche un gagnant sur la colonne
        if (plateau.cases[(id%4)].pion != null && plateau.cases[4+(id%4)].pion != null &&
               plateau.cases[8+(id%4)].pion != null && plateau.cases[12+(id%4)].pion != null)
        {
            if (
                    ((plateau.cases[(id % 4)].pion.petit == plateau.cases[4 + (id % 4)].pion.petit) &&
                        (plateau.cases[4 + (id % 4)].pion.petit == plateau.cases[8 + (id % 4)].pion.petit) &&
                        (plateau.cases[8 + (id % 4)].pion.petit == plateau.cases[12 + (id % 4)].pion.petit)) ||
                    ((plateau.cases[(id % 4)].pion.clair == plateau.cases[4 + (id % 4)].pion.clair) &&
                        (plateau.cases[4 + (id % 4)].pion.clair == plateau.cases[8 + (id % 4)].pion.clair) &&
                        (plateau.cases[8 + (id % 4)].pion.clair == plateau.cases[12 + (id % 4)].pion.clair)) ||
                    ((plateau.cases[(id % 4)].pion.rond == plateau.cases[4 + (id % 4)].pion.rond) &&
                            (plateau.cases[4 + (id % 4)].pion.rond == plateau.cases[8 + (id % 4)].pion.rond) &&
                            (plateau.cases[8 + (id % 4)].pion.rond == plateau.cases[12 + (id % 4)].pion.rond)) ||
                    ((plateau.cases[(id % 4)].pion.vide == plateau.cases[4 + (id % 4)].pion.vide) &&
                            (plateau.cases[4 + (id % 4)].pion.vide == plateau.cases[8 + (id % 4)].pion.vide) &&
                            (plateau.cases[8 + (id % 4)].pion.vide == plateau.cases[12 + (id % 4)].pion.vide)))
            {
                winner = true;
            }
        }





        //cherche un gagnant sur la ligne
        if (plateau.cases[id - (id % 4)].pion != null && plateau.cases[id - (id % 4) + 1].pion != null &&
                plateau.cases[id - (id % 4) + 2].pion != null && plateau.cases[id - (id % 4) + 3].pion != null)
        {
            if (
                    ((plateau.cases[id - (id % 4)].pion.petit == plateau.cases[id - (id % 4) + 1].pion.petit) &&
                        (plateau.cases[id - (id % 4) + 1].pion.petit == plateau.cases[id - (id % 4) + 2].pion.petit) &&
                        (plateau.cases[id - (id % 4) + 2].pion.petit == plateau.cases[id - (id % 4) + 3].pion.petit)) ||
                    ((plateau.cases[id - (id % 4)].pion.clair == plateau.cases[id - (id % 4) + 1].pion.clair) &&
                        (plateau.cases[id - (id % 4) + 1].pion.clair == plateau.cases[id - (id % 4) + 2].pion.clair) &&
                        (plateau.cases[id - (id % 4) + 2].pion.clair == plateau.cases[id - (id % 4) + 3].pion.clair)) ||
                    ((plateau.cases[id - (id % 4)].pion.rond == plateau.cases[id - (id % 4) + 1].pion.rond) &&
                        (plateau.cases[id - (id % 4) + 1].pion.rond == plateau.cases[id - (id % 4) + 2].pion.rond) &&
                        (plateau.cases[id - (id % 4) + 2].pion.rond == plateau.cases[id - (id % 4) + 3].pion.rond)) ||
                    ((plateau.cases[id - (id % 4)].pion.vide == plateau.cases[id - (id % 4) + 1].pion.vide) &&
                        (plateau.cases[id - (id % 4) + 1].pion.vide == plateau.cases[id - (id % 4) + 2].pion.vide) &&
                        (plateau.cases[id - (id % 4) + 2].pion.vide == plateau.cases[id - (id % 4) + 3].pion.vide)))

            {
                winner = true;
            }
        }

        //cherche un gagnant sur la première diagonale
        if (id%5 == 0){
            if (plateau.cases[0].pion != null && plateau.cases[5].pion != null &&
                    plateau.cases[10].pion != null && plateau.cases[15].pion != null) {
                if (
                        ((plateau.cases[0].pion.petit == plateau.cases[5].pion.petit) &&
                            (plateau.cases[5].pion.petit == plateau.cases[10].pion.petit) &&
                            (plateau.cases[10].pion.petit == plateau.cases[15].pion.petit)) ||
                        ((plateau.cases[0].pion.clair == plateau.cases[5].pion.clair) &&
                            (plateau.cases[5].pion.clair == plateau.cases[10].pion.clair) &&
                            (plateau.cases[10].pion.clair == plateau.cases[15].pion.clair)) ||
                        ((plateau.cases[0].pion.rond == plateau.cases[5].pion.rond) &&
                            (plateau.cases[5].pion.rond == plateau.cases[10].pion.rond) &&
                            (plateau.cases[10].pion.rond == plateau.cases[15].pion.rond)) ||
                        ((plateau.cases[0].pion.vide == plateau.cases[5].pion.vide) &&
                            (plateau.cases[5].pion.vide == plateau.cases[10].pion.vide) &&
                            (plateau.cases[10].pion.vide == plateau.cases[15].pion.vide))) {
                    winner = true;
                }
            }
        }

        //cherche un gagnant sur la deuxième diagonale
        else if (id%3 == 0){
            if (plateau.cases[3].pion != null && plateau.cases[6].pion != null &&
                    plateau.cases[9].pion != null && plateau.cases[12].pion != null) {
                if (
                        ((plateau.cases[12].pion.petit == plateau.cases[3].pion.petit) &&
                            (plateau.cases[3].pion.petit == plateau.cases[6].pion.petit) &&
                            (plateau.cases[6].pion.petit == plateau.cases[9].pion.petit)) ||
                        ((plateau.cases[12].pion.clair == plateau.cases[3].pion.clair) &&
                            (plateau.cases[3].pion.clair == plateau.cases[6].pion.clair) &&
                            (plateau.cases[6].pion.clair == plateau.cases[9].pion.clair)) ||
                        ((plateau.cases[12].pion.rond == plateau.cases[3].pion.rond) &&
                            (plateau.cases[3].pion.rond == plateau.cases[6].pion.rond) &&
                            (plateau.cases[6].pion.rond == plateau.cases[9].pion.rond)) ||
                        ((plateau.cases[12].pion.vide == plateau.cases[3].pion.vide) &&
                            (plateau.cases[3].pion.vide == plateau.cases[6].pion.vide) &&
                            (plateau.cases[6].pion.vide == plateau.cases[9].pion.vide))) {
                    winner = true;
                }
            }
        }
        int remplies = 0;
        if(!winner){
            for(int i = 0; i < 16; i++){
                if(plateau.cases[i].pion != null){
                    remplies ++;
                }
            }
        }

        if(winner){
             String msg = "Le " + getplayernb() + " a gagné";
            Toast toast = Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT);
            toast.show();

            //Desactivation de tous les onclick listener de pions

            for (int i = 0; i < 16; i++) {
                String str_btn = "btn_pion_" + (i);
                int btn_id = getResources().getIdentifier(str_btn, "id", getPackageName());
                ImageButton button = (ImageButton) findViewById(btn_id);
                button.setOnClickListener(null);
            }
            //Desactivation de tous les onclick listener de cases
            for (int i = 0; i < 16; i++) {
                String btn_str = "btn_case_" + (i);
                int btn_id = getResources().getIdentifier(btn_str, "id", getPackageName());
                ImageButton button = (ImageButton) findViewById(btn_id);
                button.setOnClickListener(null);
            }

        }
        else if(!winner && remplies == 16){
            String msg = "Egalité !";
            Toast toast = Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT);
            toast.show();

            //Desactivation de tous les onclick listener de pions

            for (int i = 0; i < 16; i++) {
                String str_btn = "btn_pion_" + (i);
                int btn_id = getResources().getIdentifier(str_btn, "id", getPackageName());
                ImageButton button = (ImageButton) findViewById(btn_id);
                button.setOnClickListener(null);
            }
            //Desactivation de tous les onclick listener de cases
            for (int i = 0; i < 16; i++) {
                String btn_str = "btn_case_" + (i);
                int btn_id = getResources().getIdentifier(btn_str, "id", getPackageName());
                ImageButton button = (ImageButton) findViewById(btn_id);
                button.setOnClickListener(null);
            }
        }


    }

    public String getplayernb(){
        if(activePlayer == true){
            return "Joueur 1";
        }
        else{
            return "Joueur 2";
        }
    }


} //fin main activity